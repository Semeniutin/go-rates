package apiserver

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/Semeniutin/rates-rest-api/intenral/app/store"
)


type server struct {
	router *mux.Router
	store store.Store
}

func newServer(store store.Store) *server {
	s := &server { 
		router: mux.NewRouter(),
		store: store,
	}

	s.configureRouter()
	return s
}

func (s *server) ServeHTTP (w http.ResponseWriter, r *http.Request) { 
	s.router.ServeHTTP(w, r)
}

func (s *server) configureRouter() { 
	s.router.HandleFunc("/rates", s.handleRatesRead()).Methods("GET")
}


func (s *server) handleRatesRead() http.HandlerFunc { 
	return func(w http.ResponseWriter, r *http.Request) { 
		keys := r.URL.Query()

		var required = map[string]map[string]string{ 
			"token": {"message": "token should be declared in request params"},
			"start_date": {"message": "start_date should be declared in request params"},
			"end_date": {"message": "end_date should be declared in request params"},
		}

		for k, v := range required {
			rv, ok := keys[k]
			required[k] = make(map[string]string)
			if !ok { 
				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusBadRequest)
				res := fmt.Sprintf(`{"message": %s}`, v["message"])
				w.Write([]byte(res))
				return
			}
			required[k]["obj"] = rv[0]
		}
		
		
		start_date := required["start_date"]["obj"]
		end_date := required["end_date"]["obj"]

		_, err := s.store.Token().FindByToken(required["token"]["obj"])
		if err != nil { 
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte(`{"message": "token is not valid"}`))
			return
		}
		cached := false
		max_date, min_date, err := s.store.Rates().GetExtremums()
		if err == nil { 
			cached = max_date >= end_date && min_date <= start_date
		}

		var rates []map[string]string

		if cached { 
			rates = s.store.Rates().GetRates(start_date, end_date)
		} else {
			rates = s.store.Rates().FetchRates(start_date, end_date, required["token"]["obj"], remoteServerURL)
		}

		// To export into JSON field names should be capitalized
		type Response struct { 
			Status string `json:"status"`
			Data []map[string]string `json:"data"`
		}
		rv := Response{"OK", rates}
		b, err := json.Marshal(rv)

		if rates == nil || err != nil {
			if err != nil { 
				log.Println(err)
			}
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte(`{"message": "something went wrong"}`))
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(b)
	}
}