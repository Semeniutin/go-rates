package sqlstore

import (
	"gitlab.com/Semeniutin/rates-rest-api/intenral/app/model"
)

// RatesRepository ...
type TokenRepository struct {
	store *Store
}

func (r *TokenRepository) FindByToken(token string) (*model.Token, error) {
	t := &model.Token{}

	if err := r.store.db.QueryRow(
		"SELECT id FROM token WHERE token = $1", 
		token,
	).Scan(&t.ID); err != nil { 
		return nil, err
	}
	return t, nil
}