package store

// Store ...
type Store interface {
	Rates() RatesRepository
	Token() TokenRepository
}