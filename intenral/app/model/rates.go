package model

import "time"

type Rates struct {
	ParseDate  time.Time
	CurrencyFrom string
	Currencyto   string
	Rate       string
}
