package main

import (
	"flag"
	"log"
	"os"

	"github.com/BurntSushi/toml"
	"gitlab.com/Semeniutin/rates-rest-api/intenral/app/apiserver"
)


var ( 
	configPath string
)

func init() { 
	flag.StringVar(&configPath, "config-path", "configs/apiserver.toml", "path to config file")
}

func main() {
	flag.Parse()

	f, err := os.OpenFile("text.log",
	os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	log.SetOutput(f)

	config := apiserver.NewConfig()
	_, err = toml.DecodeFile(configPath, config)
	if err != nil { 
		log.Fatal(err)
	}

	if err := apiserver.Start(config); err != nil { 
		log.Fatal(err)
	}
}